﻿using System;
using System.Diagnostics;
using System.IO;
using System.ServiceProcess;
using System.Threading;

namespace FileMoverService
{
    public partial class Service1 : ServiceBase
    {
        private static FileSystemWatcher _fileSystemWatcher = new FileSystemWatcher("C:\\ADN");
        public Service1()
        {
            InitializeComponent();

            _fileSystemWatcher.EnableRaisingEvents = true;
            _fileSystemWatcher.Changed += OnChange;
            _fileSystemWatcher.Created += OnCreate;
            _fileSystemWatcher.Renamed += OnRename;
        }

        protected override void OnStart(string[] args)
        {
            EventLog eventLog = new EventLog();
            eventLog.Source = "Application";
            eventLog.WriteEntry("File Mover Service Started.", EventLogEntryType.Information);
        }

        protected override void OnStop()
        {
            EventLog eventLog = new EventLog();
            eventLog.Source = "Application";
            eventLog.WriteEntry("File Mover Service Stopped.", EventLogEntryType.Information);
        }


        static void MoveFile(string sourcePath, string destinationPath)
        {
            if (File.Exists(sourcePath))
            {
                Thread.Sleep(1000);
                if (File.Exists(destinationPath))
                {
                    File.Delete(destinationPath);
                }
                File.Move(sourcePath, destinationPath);

                Console.WriteLine($"File moved from {sourcePath} to {destinationPath}");
            }
        }

        static void OnChange(object sender, FileSystemEventArgs e)
        {
            EventLog eventLog = new EventLog();
            eventLog.Source = "Application";
            eventLog.WriteEntry("File moved.", EventLogEntryType.Information);
        }

        static void OnCreate(object sender, FileSystemEventArgs e)
        {
            try
            {
                MoveFile(e.FullPath, $"C:\\ADN\\Target\\{e.Name}");
            }
            catch
            {
                EventLog eventLog = new EventLog();
                eventLog.Source = "Application";
                eventLog.WriteEntry("File could not be moved", EventLogEntryType.Error);
            }
        }

        static void OnRename(object sender, FileSystemEventArgs e)
        {
            try
            {
                MoveFile(e.FullPath, $"C:\\ADN\\Target\\{e.Name}");
            }
            catch
            {
                EventLog eventLog = new EventLog();
                eventLog.Source = "Application";
                eventLog.WriteEntry("File could not be moved", EventLogEntryType.Error);
            }
        }
    }
}
