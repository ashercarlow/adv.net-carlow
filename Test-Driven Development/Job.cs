﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test_Driven_Development
{
    [Serializable]
    public class Job
    {
        private Job() { }

        public Job(int timeInvestment)
        {
            TimeInvestmentRemaining = timeInvestment;
        }

        public int TimeInvestmentRemaining { get; set; }
        public decimal JobCost { get; set; }

        public bool JobCompleted => TimeInvestmentRemaining == 0;
    }
}
