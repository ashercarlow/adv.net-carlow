﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test_Driven_Development
{
    [Serializable]
    public class Business
    {
        public Business()
        {
            Employees = new List<Employee>();
            Jobs = new List<Job>();
        }

        public List<Employee> Employees { get; set; }

        public List<Job> Jobs { get; set; }

        public void AddEmployee(Employee employee)
        {
            Employees.Add(employee);
        }

        public void AddJob(Job job)
        {
            Jobs.Add(job);
        }

        public void DoWork()
        {
            // For each job, if job isn't completed, check each employees to see if they have hours and have them DoWork(). If job is completed after work, break from loop.
            foreach(Job j in Jobs)
            {
                if (!j.JobCompleted)
                {
                    foreach(Employee e in Employees)
                    {
                        e.DoWork(j);
                        if (j.JobCompleted) break;
                    }
                }
            }
        }
    }
}
