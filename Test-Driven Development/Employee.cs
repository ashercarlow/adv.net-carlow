﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test_Driven_Development
{
    [Serializable]
    public class Employee
    {
        private decimal _hourlyWage;

        private int _hoursScheduled;

        private Employee() { }

        public Employee(decimal hourlyWage, int hoursScheduled)
        {
            _hourlyWage = hourlyWage;
            _hoursScheduled = hoursScheduled;
        }

        public decimal Paycheck { get; set; }

        public void DoWork(Job work)
        {
            // If the hoursScheduled are less than the time remaining on the job, reduce the hours remaining on the job by the hours scheduled, and set hours scheduled to zero.
            // Else reduce hours scheduled by time remaining, and set time investment for job to 0.
            if (_hoursScheduled < work.TimeInvestmentRemaining)
            {
                work.TimeInvestmentRemaining -= _hoursScheduled;
                Paycheck += _hoursScheduled * _hourlyWage;
                work.JobCost += _hoursScheduled * (_hourlyWage * 1.5m);
                _hoursScheduled = 0;
            }
            else
            {
                _hoursScheduled -= work.TimeInvestmentRemaining;
                Paycheck += work.TimeInvestmentRemaining * _hourlyWage;
                work.JobCost += work.TimeInvestmentRemaining * (_hourlyWage * 1.5m);
                work.TimeInvestmentRemaining = 0;
            }
        }
    }
}
